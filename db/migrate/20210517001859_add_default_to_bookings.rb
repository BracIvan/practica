class AddDefaultToBookings < ActiveRecord::Migration[6.0]
  def change
        change_column :users, :role, :string, :default => "Visitante"
        change_column :bookings, :state, :string, :default => "Creada"
  end
end
