Rails.application.routes.draw do
  get 'home/index'
  resources :bookings
  devise_for :users
  resources :users
  resources :books
  root to: 'home#index'
  get 'home', to: 'home#index'
  delete '/books/:id', to: 'books#destroy'


#  get "/sign_out" => "devise/sessions#destroy", :as => :destroy_user_session
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
