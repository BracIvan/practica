class Book < ApplicationRecord
  validates :name, presence: true
  validates :isbn, presence: true
  validates :year, presence: true
  validates :editorial, presence: true
end
