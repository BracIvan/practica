class Booking < ApplicationRecord
  validates :book_id, presence: true
  validates :user_dni, presence: true, length: { minimum: 6 }
  validates :start_booking, presence: true
  validates :start_date, presence: true
  validates :end_time, presence: true
end
