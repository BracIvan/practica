json.extract! user, :id, :first_name, :last_name, :dni, :phone, :email, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
