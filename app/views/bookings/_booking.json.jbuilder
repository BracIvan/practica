json.extract! booking, :id, :book_id, :user_dni, :start_booking, :start_date, :end_time, :state, :created_at, :updated_at
json.url booking_url(booking, format: :json)
